import { browser, logging } from 'protractor';

import { HomePage } from '../home/home.po';
import { PhotoUploadPage } from './photo-upload.po';

describe('Photo upload page', () => {
  let homePage: HomePage;
  let photoUploadPage: PhotoUploadPage;

  afterEach(async () => {
    const logs = browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(
      jasmine.objectContaining({ level: logging.Level.SEVERE } as logging.Entry)
    );
  });

  beforeEach(async () => {
    photoUploadPage = new PhotoUploadPage();
    await photoUploadPage.navigateTo();
  });

  it('Should navigate to photo upload page', async () => {
    const title = await photoUploadPage.getTitle();
    expect(title).toBe(PhotoUploadPage.PAGE_TITLE);
  });

  it('Should upload photo with comment', async () => {
    await photoUploadPage.selectImage();
    await photoUploadPage.fillDescription('Some description');
    await photoUploadPage.upload();
    const title = await photoUploadPage.getTitle();
    expect(title).toEqual(HomePage.PAGE_TITLE);
  });
});
