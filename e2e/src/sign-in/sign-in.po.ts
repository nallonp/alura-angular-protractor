import { browser, by, element } from 'protractor';

export class SignInPage {
  static PAGE_TITLE = 'Sign in';

  getTitle() {
    return browser.getTitle();
  }

  fillUserNameField(userName: string) {
    return element(by.css('input[formcontrolname=userName]')).sendKeys(
      userName
    );
  }

  fillPasswordField(password: string) {
    return element(by.css('input[formcontrolname=password]')).sendKeys(
      password
    );
  }

  login() {
    return element(by.css('button[type=submit]')).click();
  }
}
