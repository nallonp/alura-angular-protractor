import { browser, logging } from 'protractor';
import { SignUpPage } from './sign-up.po';
import { SignInPage } from '../sign-in/sign-in.po';
import { HomePage } from '../home/home.po';

describe('Sign up page', () => {
  let signUpPage: SignUpPage;
  let signInPage: SignInPage;
  let homePage: HomePage;

  afterEach(async () => {
    const logs = browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(
      jasmine.objectContaining({ level: logging.Level.SEVERE } as logging.Entry)
    );
  });

  beforeEach(async () => {
    signUpPage = new SignUpPage();
    signInPage = new SignInPage();
    homePage = new HomePage();
    await signUpPage.navigateTo();
  });

  it('Should be on SignUp Page', async () => {
    const title = await signUpPage.getTitle();
    expect(title).toBe(SignUpPage.PAGE_TITLE);
  });

  it('Should register a user', async () => {
    const randomPrefix = Math.round(Math.random() * 100000);
    await signUpPage.fillField('email', `email${randomPrefix}@email.com`);
    await signUpPage.fillField('fullName', `Some name ${randomPrefix}`);
    const userName = `user${randomPrefix}`;
    await signUpPage.fillField('userName', userName);
    const password = '1234567890';
    await signUpPage.fillField('password', password);
    await signUpPage.register();
    let title = await signInPage.getTitle();
    expect(title).toEqual(SignInPage.PAGE_TITLE);
    await signInPage.fillUserNameField(userName);
    await signInPage.fillPasswordField(password);
    await signInPage.login();
    title = await homePage.getTitle();
    expect(title).toEqual(HomePage.PAGE_TITLE);
  });
});
