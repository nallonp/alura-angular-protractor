import { browser, by, element } from 'protractor';

export class SignUpPage {
  static PAGE_TITLE = 'Sign up';

  navigateTo() {
    return browser.get(`${browser.baseUrl}#/home/signup`);
  }

  getTitle() {
    return browser.getTitle();
  }

  fillField(control: string, text: string) {
    return element(by.formControlName(control)).sendKeys(text);
    /* return element(
      by.css(`input[formcontrolname=${formControlName}]`)
    ).sendKeys(text);*/
  }

  /* fillEmailField(text: string) {
    return element(by.css('input[formcontrolname=email]')).sendKeys(text);
  }

  fillFullNameField(fullName: string) {
    return element(by.css('input[formcontrolname=fullName]')).sendKeys(
      fullName
    );
  }

  fillUserNameField(userName: string) {
    return element(by.css('input[formcontrolname=userName]')).sendKeys(
      userName
    );
  }

  fillPasswordField(password: string) {
    return element(by.css('input[formcontrolname=password]')).sendKeys(
      password
    );
  }*/

  register() {
    return element(by.css('button[type=submit]')).click();
  }
}
